FROM python

WORKDIR  /var/local/ficarico/

COPY . ./

RUN pip3 install --upgrade pip \
    && pip3 install -r requirements.txt

CMD bin/backend
