# Makefile
PYTHON ?= python
PYLINT ?= pylint

SETUP := $(PYTHON) setup.py
APP := ./bin/backend

all: build

build:
	$(SETUP) build

dist: all
	$(SETUP) bdist_egg

install: all
	$(SETUP) install

clean:
	$(SETUP) clean
	rm -rf build dist *.egg-info

check: all lint test

test:
	$(SETUP) test

lint:
	$(PYLINT) backend setup tests

debug:
	$(APP) -d -p 8080

help:
	@echo "Makefile"
	@echo
	@echo "Usage: make [TARGET]"
	@echo
	@echo "Available targets:"
	@echo "  all       (default) Build the project."
	@echo "  build     Build the project."
	@echo "  dist      Build distribution package (egg)."
	@echo "  install   Install the package on this system."
	@echo "  clean     Remove all files created by other commands."
	@echo "  check     Performs all code validation checks."
	@echo "  lint      Verifies coding rules using Pylint."
	@echo "  test      Run unit tests."
	@echo "  debug     Run application in debug mode."
	@echo "  help      Show this help."

.PHONY: all build dist install clean check lint test debug
