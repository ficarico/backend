# -*- coding: utf-8 -*-
"""
Setup file.
"""
import setuptools
import versioneer

setuptools.setup(name="backend",
                 version=versioneer.get_version(),
                 packages=setuptools.find_packages(exclude=["tests", "tests.*"]),
                 setup_requires=["pytest-runner"],
                 tests_require=["pytest", "pytest-cov", "mock>=2.0"],
                 test_suite="tests",
                 scripts=["bin/backend"],
                 install_requires=["flask>=0.10"],
                 zip_safe=False,
                 cmdclass=versioneer.get_cmdclass())
