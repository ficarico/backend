# -*- coding: utf-8 -*-
"""
  Api handlers
"""
from __future__ import division, absolute_import, print_function, unicode_literals

import json
import logging

import flask

from ..server import JSON_CONTENT

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

root_bp = flask.Blueprint('root', __name__)  # pylint: disable=invalid-name
hello_bp = flask.Blueprint('hello', __name__)  # pylint: disable=invalid-name

@root_bp.route('/', methods=['GET'])
def root_handler():
    """Handler a request to the url / """
    return json.dumps({"text": "Backend Application!"}), 200, JSON_CONTENT

@hello_bp.route('/hello', methods=['GET'])
def hello_world_handler():
    name = flask.request.args.get('name')
    """Handler a request to the url /hello """
    return json.dumps({"text": "Hello {}!".format(name)}), 200, JSON_CONTENT
