# -*- coding: utf-8 -*-
"""
  Billings handlers
"""
from __future__ import division, absolute_import, print_function, unicode_literals

import json
import logging
import flask
from bson.json_util import dumps
from pymongo import errors
import time

from ..server import JSON_CONTENT
from ..persistence import billingdao
from ..auth import requires_authorization

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

recent_bills_bp = flask.Blueprint('recentBills', __name__)  # pylint: disable=invalid-name
older_bills_bp = flask.Blueprint('olderBills', __name__)  # pylint: disable=invalid-name
generate_month_bills_bp = flask.Blueprint('generateBills', __name__)  # pylint: disable=invalid-name

prefix = '/billing'

@recent_bills_bp.route(prefix + '/getRecent', methods=['GET'])
@requires_authorization
def recent_bills_handler(requester_id):
    """Handler a request to the url /getRecent """
    bills = []
    for current in billingdao.profile_current_month_bill(requester_id):
        bills.append(current)

    for old in billingdao.profile_closed_bills(requester_id, 1):
        bills.append(old)

    return dumps(bills), 200, JSON_CONTENT

@older_bills_bp.route(prefix + '/getOld', methods=['GET'])
@requires_authorization
def older_bills_handler(requester_id):
    """Handler a request to the url /getOld """
    # result = billingdao.generate_last_month_bills()
    result = billingdao.profile_closed_bills(requester_id, 2)

    # time.sleep(2)

    return dumps(result), 200, JSON_CONTENT

@generate_month_bills_bp.route(prefix + '/generateBills', methods=['GET'])
@requires_authorization
def generate_month_bills_handler(requester_id):
    month = flask.request.args.get('month', type=int)

    result = billingdao.generate_last_month_bills(month)

    return dumps(result), 200, JSON_CONTENT
