# -*- coding: utf-8 -*-
"""
  profile handlers
"""
from __future__ import division, absolute_import, print_function, unicode_literals

import json
import logging
import flask
import time
import random
from pymongo import errors
from bson.json_util import dumps

from ..server import JSON_CONTENT
from ..persistence import profiledao, passwordresetdao
from ..auth import encode_auth_token, requires_authorization
from ..utils import send_reset_password_email

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

profile_follow_bp = flask.Blueprint('profileFollow', __name__) # pylint: disable=invalid-name
login_bp = flask.Blueprint('login', __name__) # pylint: disable=invalid-name
login_with_facebook_bp = flask.Blueprint('loginWithFacebook', __name__) # pylint: disable=invalid-name
sign_up_bp = flask.Blueprint('signUp', __name__) # pylint: disable=invalid-name
profiles_suggestions_bp = flask.Blueprint('profilesSuggestions', __name__) # pylint: disable=invalid-name
add_hashtags_profile_bp = flask.Blueprint('addHashtags', __name__) # pylint: disable=invalid-name
get_profile_bp = flask.Blueprint('getProfile', __name__) # pylint: disable=invalid-name
password_reset_request_bp = flask.Blueprint('passwordResetRequest', __name__) # pylint: disable=invalid-name
change_password_bp = flask.Blueprint('changePassword', __name__) # pylint: disable=invalid-name
profiles_search_bp = flask.Blueprint('profilesSearch', __name__) # pylint: disable=invalid-name

prefix = '/profile'

@login_bp.route(prefix + '/login', methods=['GET'])
def login_handler():
    """Handler a request to the url /login. Should be used for non-facebook logins"""
    email = flask.request.args.get('email')
    password = flask.request.args.get('password')
    profile_type = flask.request.args.get('type', type=int)

    # profiledao.generate_hash(email, password)
    logged_profile = profiledao.verify_user_login(None, email, password, profile_type)

    if logged_profile is None:
        # time.sleep(3)
        return dumps('Incorrect username or password'), 401, JSON_CONTENT

    logged_profile['auth_token'] = encode_auth_token(str(logged_profile.get('_id')))

    return dumps(logged_profile), 200, JSON_CONTENT

@login_with_facebook_bp.route(prefix + '/loginWithFacebook', methods=['POST'])
def login_with_facebook_handler():
    """Handler a request to the url /loginWithFacebook. Should be used for facebook login and facebook registration"""
    profile = flask.request.get_json()

    try:
        result = None
        returned_id = profile.get('_id')

        user = profile.get('user')
        result = profiledao.verify_user_login(user.get('facebook_id'), None, None, None)

        if result is None:
            updated = profiledao.update_profile(profile)
            if updated['updatedExisting'] is False:
                inserted = profiledao.insert_profile(profile)
                returned_id = inserted.inserted_id
            result = profiledao.profile_by_id(returned_id)

        result['auth_token'] = encode_auth_token(str(result.get('_id')))

        return dumps(result), 200, JSON_CONTENT
    except errors.DuplicateKeyError as e:
        return dumps(e.details), 422, JSON_CONTENT
    except errors.WriteError as e:
        return dumps(e.details), 400, JSON_CONTENT

@sign_up_bp.route(prefix + '/signUp', methods=['POST'])
def sign_up_handler():
    """Handler a request to the url /register. Should be used for ordinary register aka non-facebook"""
    profile = flask.request.get_json()

    try:
        id = profile.get('_id')
        updated = profiledao.update_profile(profile)

        if updated['updatedExisting'] is False:
            logger.info("PASSED HERE")
            inserted = profiledao.insert_profile(profile)
            id = inserted.inserted_id

        result = profiledao.profile_by_id(id)
        result['auth_token'] = encode_auth_token(str(result.get('_id')))

        return dumps(result), 200, JSON_CONTENT
    except errors.DuplicateKeyError as e:
        return dumps(e.details), 422, JSON_CONTENT
    except errors.WriteError as e:
        return dumps(e.details), 400, JSON_CONTENT

@get_profile_bp.route(prefix + '/get', methods=['GET'])
@requires_authorization
def get_profile_handler(requester_id):
    """Handler a request to the url /get"""

    return dumps(profiledao.profile_by_id(requester_id)), 200, JSON_CONTENT

@profile_follow_bp.route(prefix + '/follow', methods=['POST'])
@requires_authorization
def profile_follow_handler(requester_id):
    """Handler a request to the url /profileFollow"""
    form = flask.request.form
    result = profiledao.follow_profile(requester_id, form.get('profile_followed'), form.get('follow'))

    return dumps(result), 200, JSON_CONTENT

@profiles_suggestions_bp.route(prefix + '/profilesSuggestions', methods=['GET'])
@requires_authorization
def profiles_suggestions_handler(requester_id):
    """Handler a request to the url /profilesSuggestions"""
    latitude = flask.request.args.get('latitude', type=float)
    longitude = flask.request.args.get('longitude', type=float)

    profile = profiledao.profiles_and_tags_followed_by_id(requester_id)
    result = profiledao.profiles_suggestions(profile, latitude, longitude)

    return dumps(result), 200, JSON_CONTENT

@add_hashtags_profile_bp.route(prefix + '/addHashtags', methods=['POST'])
@requires_authorization
def add_hashtags_handler(requester_id):
    """Handler a request to the url /addHashtags"""
    try:
        json = flask.request.get_json()
        result = profiledao.add_hashtags_profile(requester_id, json.get('hashtags'))

        return dumps(result), 200, JSON_CONTENT
    except errors.WriteError as e:
        return dumps(e.details), 400, JSON_CONTENT

@password_reset_request_bp.route(prefix + '/passwordResetRequest', methods=['POST'])
def password_reset_request_handler():
    form = flask.request.form
    email = form.get('email')
    profile_type = form.get('profile_type', type=int)

    profile = profiledao.profile_by_email(email, profile_type)

    if profile is not None:
        key = random.randint(0, 999999)
        passwordresetdao.insert(key, email, profile_type)
        problems = send_reset_password_email(email, key)

        if problems:
            return dumps({'ERROR': problems}), 400, JSON_CONTENT

        return dumps('OK'), 200, JSON_CONTENT

    return dumps('Email not found'), 401, JSON_CONTENT

@change_password_bp.route(prefix + '/changePassword', methods=['POST'])
def change_password_handler():
    form = flask.request.form
    new_password = form.get('new_password')
    key = form.get('key', type=int)

    reset = passwordresetdao.get_valid(key)
    if reset is not None:
        profile = profiledao.profile_by_email(reset.get('_id'), reset.get('profile_type'))
    else:
        email = form.get('email')
        old_password = form.get('old_password')
        profile_type = form.get('profile_type', type=int)
        profile = profiledao.verify_user_login(None, email, old_password, profile_type)

    if profile is not None:
        profile['user']['password'] = new_password
        profiledao.update_profile(profile)
        passwordresetdao.use_key(key)

        return dumps('OK'), 200, JSON_CONTENT

    return dumps('ERROR'), 401, JSON_CONTENT

@profiles_search_bp.route(prefix + '/search', methods=['GET'])
@requires_authorization
def profiles_search_handler(requester_id):
    """Handler a request to the url /search"""
    latitude = flask.request.args.get('latitude', type=float)
    longitude = flask.request.args.get('longitude', type=float)
    text = flask.request.args.get('text')

    profiles = profiledao.search_profiles(text)

    return dumps(profiles), 200, JSON_CONTENT
