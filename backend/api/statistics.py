# -*- coding: utf-8 -*-
"""
  Statistics handlers
"""
from __future__ import division, absolute_import, print_function, unicode_literals

import json
import logging
import flask
from bson.json_util import dumps
from pymongo import errors
import time

from ..server import JSON_CONTENT
from ..persistence import statisticsdao
from ..auth import requires_authorization

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

visualization_bp = flask.Blueprint('visualization', __name__)  # pylint: disable=invalid-name
click_bp = flask.Blueprint('click', __name__)  # pylint: disable=invalid-name
publications_statistics_bp = flask.Blueprint('publications', __name__) # pylint: disable=invalid-name
profile_general_statistics_bp = flask.Blueprint('profile', __name__) # pylint: disable=invalid-name
profile_statistics_by_period_bp = flask.Blueprint('byPeriod', __name__) # pylint: disable=invalid-name

prefix = '/statistics'

@visualization_bp.route(prefix + '/visualization', methods=['POST'])
@requires_authorization
def visualization_handler(requester_id):
    """Handler a request to the url /visualization """
    visualizations = flask.request.get_json()

    try:
        # time.sleep(10)
        result = statisticsdao.insert_visualizations(visualizations)

        return dumps(result), 200, JSON_CONTENT
    except errors.BulkWriteError as e:
        return dumps(e), 400, JSON_CONTENT

@click_bp.route(prefix + '/click', methods=['POST'])
@requires_authorization
def click_handler(requester_id):
    """Handler a request to the url /click """
    clicks = flask.request.get_json()

    try:
        # time.sleep(5)
        result = statisticsdao.insert_clicks(clicks)

        return dumps(result), 200, JSON_CONTENT
    except errors.BulkWriteError as e:
        return dumps(e), 400, JSON_CONTENT

@publications_statistics_bp.route(prefix + '/publications', methods=['GET'])
@requires_authorization
def publications_statistics_handler(requester_id):
    """Handler a request to the url /publications """
    result = statisticsdao.profile_publications_statistics(requester_id)

    return dumps(result), 200, JSON_CONTENT

@profile_general_statistics_bp.route(prefix + '/profile', methods=['GET'])
@requires_authorization
def profile_general_statistics_handler(requester_id):
    result = statisticsdao.profile_general_statistics(requester_id)

    return dumps(result), 200, JSON_CONTENT

@profile_statistics_by_period_bp.route(prefix + '/byPeriod', methods=['GET'])
@requires_authorization
def statistics_by_period_handler(requester_id):
    """Handler a request to the url /profile """
    type = flask.request.args.get('type')

    result = statisticsdao.profile_statics_by_period(requester_id, type)

    # time.sleep(2)

    return dumps(result), 200, JSON_CONTENT
