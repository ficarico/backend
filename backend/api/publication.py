# -*- coding: utf-8 -*-
"""
  Publications handlers
"""
from __future__ import division, absolute_import, print_function, unicode_literals

import json
import logging
import flask
from bson.json_util import dumps
from pymongo import errors
import time
import jwt

from ..server import JSON_CONTENT
from ..persistence import publicationdao, profiledao, hashtagdao, statisticsdao
from ..auth import requires_authorization

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

feed_bp = flask.Blueprint('feed', __name__)  # pylint: disable=invalid-name
publication_by_profile_bp = flask.Blueprint('publicationsByProfile', __name__) # pylint: disable=invalid-name
publications_search_bp = flask.Blueprint('publicationsSearch', __name__) # pylint: disable=invalid-name
publication_like_bp = flask.Blueprint('publicationLike', __name__) # pylint: disable=invalid-name
create_publication_bp = flask.Blueprint('createPublication', __name__) # pylint: disable=invalid-name
change_active_status_bp = flask.Blueprint('changeActiveStatus', __name__) # pylint: disable=invalid-name

prefix = '/publication'

@feed_bp.route(prefix + '/feed', methods=['GET'])
@requires_authorization
def feed_handler(requester_id):
    """Handler a request to the url /feed"""
    page = flask.request.args.get('page', type=int)
    profile = profiledao.profiles_and_tags_followed_by_id(requester_id)
    feed = []

    if profile is not None:
        feed = publicationdao.get_user_feed(profile, page)

    return dumps(feed), 200, JSON_CONTENT

@publication_by_profile_bp.route(prefix + '/getByProfileId', methods=['GET'])
@requires_authorization
def publication_by_profile_handler(requester_id):
    """Handler a request to the url /getByProfileId"""
    id = flask.request.args.get('id')
    pub_list = publicationdao.list_of_publications_by_profile_id(id)

    return dumps(pub_list), 200, JSON_CONTENT

@publications_search_bp.route(prefix + '/search', methods=['GET'])
@requires_authorization
def publications_search_handler(requester_id):
    """Handler a request to the url /search"""
    latitude = flask.request.args.get('latitude', type=float)
    longitude = flask.request.args.get('longitude', type=float)
    page = flask.request.args.get('page', type=int)
    text = flask.request.args.get('text')

    try:
        relevant_publications = None
        profile = profiledao.profiles_and_tags_followed_by_id(requester_id)

        if text.strip():
            profiles_nearby = profiledao.get_nearby_profiles(latitude, longitude)
            relevant_publications = publicationdao.search_publications_from_profiles(profile, text, profiles_nearby, page)
        else:
            relevant_publications = publicationdao.get_publications_from_nearby_profiles(profile, latitude, longitude, page)

        return dumps(relevant_publications), 200, JSON_CONTENT
    except errors.PyMongoError as e:
        return dumps(e.details), 400, JSON_CONTENT

@publication_like_bp.route(prefix + '/like', methods=['POST'])
@requires_authorization
def publication_like_handler(requester_id):
    """Handler a request to the url /like"""
    form = flask.request.form
    result = publicationdao.like_publication(requester_id, form.get('publication_liked'), form.get('like'))

    return dumps(result), 200, JSON_CONTENT

@create_publication_bp.route(prefix + '/createOrUpdate', methods=['POST'])
@requires_authorization
def create_publication_handler(requester_id):
    """Handler a request to the url /createPublication"""
    publication = flask.request.get_json()

    try:
        hashtagdao.update_hashtags(publication.get('tags'))
        id = publication.get('_id')

        if id:
            result = publicationdao.update_publication(requester_id, publication)
        else:
            inserted = publicationdao.insert_publication(requester_id, publication)
            id = inserted.inserted_id

        # time.sleep(5)
        return dumps(statisticsdao.publication_statistic(id)), 200, JSON_CONTENT
    except errors.WriteError as e:
        # time.sleep(5)
        return dumps(e.details), 400, JSON_CONTENT

@change_active_status_bp.route(prefix + '/changeStatus', methods=['POST'])
@requires_authorization
def change_active_status_handler(requester_id):
    """Handler a request to the url /like"""

    json = flask.request.get_json()

    try:
        result = publicationdao.change_active_status(json.get('publication_id'), json.get('status'))
        return dumps(result), 200, JSON_CONTENT
    except errors.WriteError as e:
        return dumps(e.details), 400, JSON_CONTENT
