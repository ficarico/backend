# -*- coding: utf-8 -*-
"""
  Statistics handlers
"""
from __future__ import division, absolute_import, print_function, unicode_literals

import json
import logging
import flask
from bson.json_util import dumps
from pymongo import errors
import time

from ..server import JSON_CONTENT
from ..persistence import reportdao
from ..auth import requires_authorization

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

insert_report_bp = flask.Blueprint('insertReport', __name__)  # pylint: disable=invalid-name
get_reports_bp = flask.Blueprint('getReports', __name__)  # pylint: disable=invalid-name
get_report_types_bp = flask.Blueprint('getReportTypes', __name__) # pylint: disable=invalid-name

prefix = '/report'

@insert_report_bp.route(prefix + '/insert', methods=['POST'])
@requires_authorization
def insert_report_handler(requester_id):
    """Handler a request to the url /insert """
    report = flask.request.get_json()

    try:
        result = reportdao.insert_report(requester_id, report)

        return dumps({'Inserted report: ': result.inserted_id}), 200, JSON_CONTENT
    except errors.WriteError as e:
        return dumps(e), 400, JSON_CONTENT

@get_reports_bp.route(prefix + '/get', methods=['GET'])
def get_reports_handler():
    """Handler a request to the url /get"""
    profile_id = flask.request.args.get('profile_id')
    publication_id = flask.request.args.get('publication_id')

    result = reportdao.get(profile_id, publication_id)

    return dumps(result), 200, JSON_CONTENT

@get_report_types_bp.route(prefix + '/getTypes', methods=['GET'])
@requires_authorization
def get_reports_handler(requester_id):
    """Handler a request to the url /getTypes"""

    result = reportdao.get_report_types()

    return dumps(result), 200, JSON_CONTENT
