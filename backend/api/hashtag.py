# -*- coding: utf-8 -*-
"""
  Hashtags handlers
"""
from __future__ import division, absolute_import, print_function, unicode_literals

import json
import logging
import flask
from pymongo import errors
from bson.json_util import dumps

from ..server import JSON_CONTENT
from ..persistence import hashtagdao, profiledao
from ..auth import requires_authorization

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

insert_hashtag_bp = flask.Blueprint('insertHashtag', __name__)  # pylint: disable=invalid-name
get_hashtags_bp = flask.Blueprint('getHashtag', __name__) # pylint: disable=invalid-name
search_hashtag_bp = flask.Blueprint('searchHashtag', __name__) # pylint: disable=invalid-name
prefix = '/hashtag'

@insert_hashtag_bp.route(prefix + '/insert', methods=['POST'])
@requires_authorization
def insert_hash_handler(requester_id):
    """Handler a request to the url insert"""
    hashtag = flask.request.get_json()

    try:
        inserted = hashtagdao.insert_hashtag(hashtag)
        id = inserted.inserted_id

        return dumps(hashtagdao.hashtag_by_id(id)), 200, JSON_CONTENT
    except errors.DuplicateKeyError as e:
        return dumps(e.details), 422, JSON_CONTENT
    except errors.WriteError as e:
        return dumps(e.details), 400, JSON_CONTENT
    except AttributeError as e:
        return dumps({"Error": "No hashtag was inserted"}), 400, JSON_CONTENT

@get_hashtags_bp.route(prefix + '/getNonFollowed', methods=['GET'])
@requires_authorization
def get_hashtags_handler(requester_id):
    """Handler a request to the url get"""
    page = flask.request.args.get('page', type=int)

    profile = profiledao.profiles_and_tags_followed_by_id(requester_id)
    hashtags = hashtagdao.get_non_followed(profile, page)

    return dumps(hashtags), 200, JSON_CONTENT

@search_hashtag_bp.route(prefix + '/search', methods=['GET'])
@requires_authorization
def search_hashtag_handler(requester_id):
    """Handler a request to the url search"""
    text = flask.request.args.get('text')
    tags = flask.request.args.getlist('tags')

    result = hashtagdao.search_hashtag(text, tags)

    return dumps(result), 200, JSON_CONTENT
