# -*- coding: utf-8 -*-

"""
Application configuration parameters.
"""
import datetime
import jwt
from functools import wraps
import flask
from bson.json_util import dumps
import logging

from .server import JSON_CONTENT

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

SECRET_KEY = 'PFOh8wmlT9kBooXk6vdbguBjMBp/erAg'

def encode_auth_token(id):
    payload = {
        'exp': datetime.datetime.utcnow() + datetime.timedelta(days=365),
        'iat': datetime.datetime.utcnow(),
        'sub': id
    }

    return jwt.encode(payload, SECRET_KEY, algorithm='HS256').decode('ascii')

def decode_auth_token(auth_token):
    payload = jwt.decode(auth_token, SECRET_KEY)

    logger.info(payload)

    return payload['sub']

def requires_authorization(f):
    @wraps(f)
    def decorated_funtion(*args, **kws):
        try:
            auth_header = flask.request.headers.get('Authorization')
            if auth_header:
                auth_token = auth_header.split(" ")[1]
                if auth_token:
                    return f(decode_auth_token(auth_token), *args, **kws)
            else:
                return dumps({'ERROR': "INVALID TOKEN"}), 401, JSON_CONTENT

        except jwt.ExpiredSignatureError as e:
            return dumps({'ERROR': "SIGNATURE EXPIRED"}), 401, JSON_CONTENT
        except jwt.InvalidTokenError as e:
            return dumps({'ERROR': "INVALID TOKEN"}), 401, JSON_CONTENT

    return decorated_funtion
