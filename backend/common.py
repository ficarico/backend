# -*- coding: utf-8 -*-
"""
  Common functions and definitions.
"""
from __future__ import division, absolute_import, print_function, unicode_literals

def dict_to_object(dictionary):
    """Converts a dict to an object, to acess his keys like attributes."""
    class Objectify(object):
        """Class that converts a dict to an object"""
        def __init__(self, dictionary):
            self.__dict__ = dictionary
    return Objectify(dictionary)
