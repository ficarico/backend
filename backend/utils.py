# -*- coding: utf-8 -*-

"""
Application utils functions.
"""

import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

LOGIN = 'xiawesome.project@gmail.com'
PASSWORD = 'Meiatoneladadec4'
GMAIL_SMTP = 'smtp.gmail.com'
PORT = 587

def send_reset_password_email(you, key):
    message = 'O seu código de redefinição de senha é: %s' % key
    subject = 'Redefinição de senha AwesomeProject'

    return send_email(LOGIN, you, [], subject, message, LOGIN, PASSWORD)

def send_email(from_addr, to_addr_list, cc_addr_list,
              subject, message,
              login, password):

    msg = MIMEMultipart()
    msg['From'] = from_addr
    msg['To'] = to_addr_list
    msg['Subject'] = subject
    msg.attach(MIMEText(message))

    server = smtplib.SMTP(GMAIL_SMTP, PORT)
    server.ehlo()
    server.starttls()
    server.login(login, password)
    problems = server.sendmail(from_addr, to_addr_list, msg.as_string())
    server.quit()

    return problems
