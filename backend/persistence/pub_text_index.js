db.getCollection('publications').createIndex({
        title: 'text',
        tags: 'text'
    },
    {
        default_language: "english",
        weights: {
            title: 2,
            tags: 1
        },
        name: 'text_search'
    }
)
