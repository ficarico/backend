# -*- coding: utf-8 -*-

from . import database
from pymongo import errors
import datetime
from bson.objectid import ObjectId
import logging

passwordresets = database.db['password_reset']
logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

EXP_TIME = 15

def get_valid(key):
    return passwordresets.find_one(
        {'$and': [
            {'key': key},
            {'exp_date': {'$gte': datetime.datetime.utcnow()}},
            {'used': False}
        ]}
    )

def insert(key, email, profile_type):
    return passwordresets.update(
        {'_id': email},
        {'$set': {
            'key': key,
            'exp_date': datetime.datetime.utcnow() + datetime.timedelta(minutes=EXP_TIME),
            'profile_type': profile_type,
            'used': False
        }},
        True
    )

def use_key(key):
    return passwordresets.update(
        {'key': key},
        {'$set': {
            'used': True
        }}
    )
