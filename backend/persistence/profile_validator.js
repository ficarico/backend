db.runCommand({
    collMod: 'profiles',
    validator: {
        $or: [
            {'user.facebook_id': {$exists: true}},
            {'user.email': {$exists: true}}
        ]
    },
    validationLevel: "off"
})
