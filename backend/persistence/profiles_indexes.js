db.profiles.createIndex(
    {'user.facebook_id': 1},
    {'unique': true, 'partialFilterExpression': {'user.facebook_id': {$type: 16}}}
)
db.profiles.createIndex(
    {'user.email': 1},
    {'unique': true, 'partialFilterExpression': {'user.email': {$type: 2}}}
)
db.profiles.createIndex(
    {'location': '2dsphere'}
)
