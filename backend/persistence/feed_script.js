db.publications.aggregate([
        {'$match':
            {'$and': 
                [
                    {'active': true},
                    {'start_date': {'$lte': new Date()}},
                    {'end_date': {'$gte': new Date()}},
                    {'$or':
                        [
                            {'profile_id': {'$in': []}},
                            {'tags': {'$in': ['bebidas', 'automovel', 'guitarra']}}
                        ]
                    }   
                ]
            }
        },
        {'$lookup':
            {
                'from': 'profiles',
                'localField': 'profile_id',
                'foreignField': '_id',
                'as': 'profile'
            }
        },
        {'$project':
            {
                'profile': { '$arrayElemAt': [ '$profile', 0 ] },
                'title': 1,
                'image': 1,
                'price': 1,
                'currency': 1,
                'tags': 1,
                'quantity': 1,
                'unit': 1,
                'information': 1,
                'start_date': 1,
                'end_date': 1,
                'likers': {'$ifNull': ['$likers', []]},
                'profile_id': 1,
            }
        },
        {'$project':
            {
                'profile_id': 0,
                'profile.profile_type': 0,
                'profile.conglomerate': 0,
                'profile.status': 0,
                'profile.profiles_followed': 0,
                'profile.tags_followed': 0,
                'profile.user.first_name': 0,
                'profile.user.last_name': 0,
                'profile.user.email': 0,
                'profile.user.password': 0,
                'profile.user.facebook_id': 0
            }
        },
        {'$addFields':
            {
                'like': {
                    '$in': [ '58ed2deba2bd932eb883d38d', '$likers' ]
                },
                'profile.following': {
                    '$in': [ '$profile._id', [] ]
                }
            }
        },
        {'$project':
            {'likers': 0}
        },
        {'$sort': {'start_date': 1, 'end_date': 1}}
    ])