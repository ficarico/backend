# -*- coding: utf-8 -*-

from . import database
from pymongo import errors
from bson.objectid import ObjectId
import pytz
import tzlocal
import datetime
import logging

reports = database.db['reports']
report_types = database.db['report_types']
logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

def insert_report(reporter_id, report):
    report['date'] = datetime.datetime.utcfromtimestamp(report.get('date')/1000)
    report['profile_id'] = ObjectId(report.get('profile_id'))
    report['publication_id'] = ObjectId(report.get('publication_id')) if report.get('publication_id') else None
    report['reporter_id'] = ObjectId(reporter_id)

    return reports.insert_one(report)

def get(profile_id, publication_id):
    find = {}
    if profile_id:
        find['profile_id'] = profile_id

    if publication_id:
        find.publication_id = publication_id

    return reports.find(find)

def get_report_types():
    return report_types.find({})
