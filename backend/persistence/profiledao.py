# -*- coding: utf-8 -*-
import os
import pysodium
from base64 import b64encode
import logging
from . import database
from pymongo import errors
from bson.objectid import ObjectId

from ..metrics import KILOMETER, REACH_RADIUS, DISTANCE_MULTIPLIER

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

profiles = database.db['profiles']
BYTES = 32

profile_fields_to_get = {
    'user.username': 1,
    'user.first_name': 1,
    'user.last_name': 1,
    'user.password': 1,
    'user.salt': 1,
    'user.email': 1,
    'user.picture': 1,
    'user.facebook_id': 1,
    'profile_type': 1
}

profile_fields_to_get_clean = {
    'user.username': 1,
    'user.first_name': 1,
    'user.last_name': 1,
    'user.email': 1,
    'user.picture': 1,
    'user.facebook_id': 1,
    'profile_type': 1
}

def get_hash_string(string, salt):
    hash_result = pysodium.crypto_pwhash(BYTES, string, salt, pysodium.crypto_pwhash_OPSLIMIT_INTERACTIVE, pysodium.crypto_pwhash_MEMLIMIT_INTERACTIVE, pysodium.crypto_pwhash_ALG_DEFAULT)
    return b64encode(hash_result).decode('utf-8')

def generate_salt():
    return b64encode(os.urandom(BYTES)).decode('utf-8')

def generate_hash(email, password):
    salt = generate_salt()
    hash = get_hash_string(password, salt)

    profiles.update({
            'user.email': email
        },
        {'$set': {
            'user.password': hash,
            'user.salt': salt
        }}
    )

def apply_hashing_on_user(user):
    if user.get('facebook_id') is None:
        salt = generate_salt()
        hash = get_hash_string(user.get('password'), salt)
        user['password'] = hash
        user['salt'] = salt

    return user

def verify_user_login(facebook_id, email, password, profile_type):
    if facebook_id is not None:
        return profiles.find_one({'user.facebook_id': facebook_id},
            profile_fields_to_get_clean
        )
    else:
        profile = profiles.find_one({'$and': [{'user.email': email}, {'profile_type.identifier': profile_type}]}, profile_fields_to_get)
        if profile is not None and profile.get('user').get('password') == get_hash_string(password, profile.get('user').get('salt')):
            profile.get('user').pop('password')
            profile.get('user').pop('salt')

            return profile
        else:
            return None

def insert_profile(profile):
    user = profile.get('user')
    profile_type = profile.get('profile_type')
    status = {
        'identifier': 1,
        'description': 'Active'
    }
    conglomerate = profile.get('conglomerate')
    address = profile.get('address')
    location = profile.get('location')
    phones = profile.get('phones') if profile.get('phones') else []
    profiles_followed = profile.get('profiles_followed') if profile.get('profiles_followed') else []
    tags_followed = profile.get('tags_followed') if profile.get('tags_followed') else []
    website = profile.get('website')

    return profiles.insert_one({
        '_id': ObjectId(),
        'user': user,
        'profile_type': profile_type,
        'status': status,
        'conglomerate': conglomerate,
        'address': address,
        'location': location,
        'phones': phones,
        'profiles_followed': profiles_followed,
        'tags_followed': tags_followed,
        'followers': 0,
        'website': website
    })

# Applies the hash on the users password regardless there is a profile to update
def update_profile(profile):
    user = profile.get('user')

    apply_hashing_on_user(user)

    id = ObjectId(profile.get('_id'))

    return profiles.update(
        {'_id': id},
        {'$set': {'user': user}}
    )

def profile_by_id(id):
    return profiles.find_one({'_id': ObjectId(id)}, profile_fields_to_get_clean)

def profile_by_email(email, profile_type):
    return profiles.find_one(
        {'$and': [
            {'user.email': email},
            {'profile_type.identifier': profile_type}
        ]},
        profile_fields_to_get_clean
    )


def profiles_and_tags_followed_by_id(id):
    return profiles.find_one({'_id': ObjectId(id)}, {'profiles_followed': 1, 'tags_followed': 1})

def add_hashtags_profile(id, hashtags):
    id = ObjectId(id)

    return profiles.update(
        {'_id': id},
        {'$addToSet': {
            'tags_followed': {
                '$each': hashtags
                }
            }
        }
    )

def follow_profile(follower, followee, follow):
    result = None
    follower = ObjectId(follower)
    followee = ObjectId(followee)

    if profile_by_id(followee) is not None:
        if follow == 'true':
            profiles.update(
                {'_id': followee},
                {'$inc': {'followers': 1}}
            )
            result = profiles.update(
                {'_id': follower},
                {'$addToSet': {'profiles_followed': followee}}
            )
        else:
            profiles.update({'_id': followee},
            {'$inc': {'followers': -1}})
            result = profiles.update({'_id': follower},
            {'$pull': {'profiles_followed': followee}})
    else:
        result = {'warning' : 'profile_followed not found'}

    return result

def get_nearby_profiles(latitude, longitude):
    return profiles.aggregate([
        {
            '$geoNear': {
                'near': { 'type': 'Point', 'coordinates': [ longitude, latitude ] },
                'distanceField': 'dist.calculated',
                'maxDistance': REACH_RADIUS * KILOMETER,
                'query': { 'status.identifier': 1 },
                'includeLocs': 'dist.location',
                'spherical': True,
                'distanceMultiplier': DISTANCE_MULTIPLIER
            }
        },
        {
            '$project': {
                'dist': 1
            }
        }
    ])

def profiles_suggestions(requester, latitude, longitude):
    return profiles.aggregate([
        {
            '$geoNear': {
                'near': { 'type': 'Point', 'coordinates': [ longitude, latitude ] },
                'distanceField': 'dist.calculated',
                'maxDistance': REACH_RADIUS * KILOMETER,
                'query': { 'status.identifier': 1 },
                'includeLocs': 'dist.location',
                'spherical': True
            }
        },
        {
            '$match': {'_id': {'$nin': requester['profiles_followed']}}
        },
        {
            '$lookup': {
                'from': 'publications',
                'localField': '_id',
                'foreignField': 'profile_id',
                'as': 'publications'
            }
        },
        {
            '$project': {
                'publications.likers': 0,
                'publications.languace': 0,
                'publications.active': 0
            }
        },
        {'$unwind': '$publications'},
        {'$sort': {'publications.start_date': -1}},
        {
            '$group': {
                '_id': '$_id',
                'user': {'$first': '$user'},
                'dist': {'$first': '$dist'},
                'address': {'$first': '$address'},
                'publications': {'$push': '$publications'}
            }
        },
        {
            '$project': {
                'user': 1,
                'dist': 1,
                'address': 1,
                'publications': {'$slice': ['$publications', 3]}
            }
        },
        {'$sort' : {'dist.calculated' : 1}}
    ])

def search_profiles(text):
    return profiles.aggregate([
        {'$match':
            {'$and':
                [
                    {'status.identifier': 1},
                    {'profile_type.identifier': 1},
                    {
                        '$text': {
                            '$search': text,
                            '$language': 'portuguese',
                            '$caseSensitive': False,
                            '$diacriticSensitive': False
                        }
                    }
                ]
            }
        },
        {'$project': {
            'user.username': 1,
            'user.picture': 1,
            'address': 1,
            'phones': 1,
            'location': 1,
            'score': { '$meta': 'textScore' }
        }},
        {'$sort': {'score': -1}},
        {'$project': {
            'score': 0
        }},
    ])
