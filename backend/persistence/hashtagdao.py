# -*- coding: utf-8 -*-

from . import database
from pymongo import errors
from bson.objectid import ObjectId
from ..metrics import HASHTAGS_PER_PAGE

hashtags = database.db['hashtags']

def insert_hashtag(hashtag):
    name = hashtag.get('name')
    tags_related = hashtag.get('tags_related')

    return hashtags.insert_one({
        '_id': name,
        'use_count': 1,
        'tags_related': tags_related,
        'active': True
    })

def hashtag_by_id(id):
    return hashtags.find_one({'_id': id})

def get_non_followed(requester, page):
    return hashtags.find(
        {'_id': {'$nin': requester['tags_followed'] }},
        {'_id': 1, 'use_count': 1}
    ).sort([('use_count', -1), ('_id', 1)]).skip(page * HASHTAGS_PER_PAGE).limit(HASHTAGS_PER_PAGE)

def update_hashtags(hashtag_list):
    for hashtag in hashtag_list:
        copy = list(hashtag_list)
        copy.remove(hashtag)

        hashtags.update(
            {'_id': hashtag},
            {
                '$inc': {'use_count': 1},
                '$addToSet': {'tags_related': {'$each': copy}},
                '$setOnInsert': {
                    '_id': hashtag,
                    'active': True,
                    'language': 'portuguese'
                }
            },
            True
        )

def search_hashtag(text, tags):
    return hashtags.find({
            '$and' : [
                {'_id': {'$regex' : text, '$options' : 'i'}},
                {'_id': {'$nin': tags}}
            ]
        },
        {'_id': 1, 'use_count': 1, 'tags_related': 1}
    )
