db.getCollection('hashtags').createIndex({
        _id: 'text',
        tags_related: 'text'
    },
    {
        default_language: "english",
        weights: {
            _id: 3,
            tags_related: 1
        },
        name: 'hashtag_search'
    }
)