# -*- coding: utf-8 -*-

from . import database
from pymongo import errors
from bson.objectid import ObjectId
from bson.codec_options import CodecOptions
import datetime
from tzlocal import get_localzone
import logging

from ..metrics import VISUALIZATION_COST

billings = database.db['billings']
publications = database.db['publications']
profiles = database.db['profiles']

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + datetime.timedelta(days=4)  # this will never fail
    return next_month - datetime.timedelta(days=next_month.day)

def profile_current_month_bill(id):
    tz = get_localzone()
    date_local = tz.localize(datetime.datetime.now())
    end_date = date_local

    month_start = tz.localize(datetime.datetime(date_local.year, date_local.month, 1, 0, 0))

    offset = int(round(month_start.utcoffset().total_seconds()/3600)) * -1

    return publications.aggregate([
        {'$match':
            {'profile_id': ObjectId(id)}
        },
        {'$lookup':
            {
                'from': 'statistics',
                'localField': '_id',
                'foreignField': 'publication_id',
                'as': 'statistics'
            }
        },
        {
            '$unwind' : '$statistics'
        },
        {
            '$replaceRoot': { 'newRoot': '$statistics' }
        },
        {'$match':
            {'date': {'$gte': month_start}}
        },
        {'$group':
            {
                '_id': {'$month': {
                    "$subtract": [
                        "$date", offset * 1000 * 60 * 60
                    ]
                }},
                'visualizations': {'$sum': '$visualizations'}
             }
        },
        {'$addFields':
            {
                '_id': ObjectId(),
                'start_date': month_start,
                'end_date': end_date,
                'status': 0,
                'offset': offset,
                'visCost': VISUALIZATION_COST,
                'total': {'$multiply': ['$visualizations', VISUALIZATION_COST]},
                'estimatedBillAmount': {'$multiply': [30, {'$divide': [{'$multiply': ['$visualizations', VISUALIZATION_COST]}, end_date.day]}]}
            }
        }
    ])

def profile_closed_bills(profile_id, status):
    return billings.find(
        {'profile_id': ObjectId(profile_id), 'status': status},
        {'month': 0, 'year': 0, 'profile_id': 0}
    ).sort('start_date', -1)

def generate_last_month_bills(month):
    advertisers = profiles.find({'profile_type.identifier': 1}, {'_id': 1})

    tz = get_localzone()
    date_local = datetime.datetime.now()

    if month is not None:
        date_local.replace(month=month + 1)
        month_to_calculate = date_local.replace(month=month + 1, day=1) - datetime.timedelta(days=1)
    else:
        month_to_calculate = date_local.replace(day=1) - datetime.timedelta(days=1)

    start_date_local = tz.localize(datetime.datetime(month_to_calculate.year, month_to_calculate.month, 1, 0, 0))
    end_date_local = tz.localize(last_day_of_month(month_to_calculate.replace(hour=23, minute=59, second=59, microsecond=999999)))

    offset = int(round(start_date_local.utcoffset().total_seconds()/3600)) * -1

    bills = []

    for profile in advertisers:
        profile_bills = publications.aggregate([
            {'$match':
                {'profile_id': ObjectId(profile['_id'])}
            },
            {'$lookup':
                {
                    'from': 'statistics',
                    'localField': '_id',
                    'foreignField': 'publication_id',
                    'as': 'statistics'
                }
            },
            {
                '$unwind' : '$statistics'
            },
            {
                '$replaceRoot': { 'newRoot': '$statistics' }
            },
            {'$match':
                {'$and':
                    [
                        {'date': {'$gte': start_date_local}},
                        {'date': {'$lte': end_date_local}}
                    ]
                }
            },
            {'$group':
                {
                    '_id': {'$month': {
                        '$subtract': [
                            '$date', offset * 1000 * 60 * 60
                        ]
                    }},
                    'visualizations': {'$sum': '$visualizations'}
                 }
            },
            {'$addFields':
                {
                    'profile_id': profile['_id'],
                    'start_date': start_date_local,
                    'end_date': end_date_local,
                    'status': 1,
                    'visCost': VISUALIZATION_COST,
                    'total': {'$multiply': ['$visualizations', VISUALIZATION_COST]}
                }
            },
            {'$project':
                {'_id': 0}
            }
        ])

        for bill in profile_bills:
            bills.append(bill)

    if bills:
        return billings.insert(bills)
    else:
        return None

def confirm_payment(bill_id):
    return billings.update_one(
        {'_id': ObjectId(bill_id)},
        {'$set': {'status': 2}}
    )
