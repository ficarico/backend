# -*- coding: utf-8 -*-
import datetime
from . import database
import logging
from bson.objectid import ObjectId
from ..metrics import KILOMETER, REACH_RADIUS, PUBLICATIONS_PER_FEED, PUBLICATIONS_PER_SEARCH

publications = database.db['publications']
profiles = database.db['profiles']
logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

def publication_by_id(id):
    return publications.find_one({'_id': ObjectId(id)})

def list_of_publications_by_profile_id(id):
    return publications.find({
        'profile_id' : ObjectId(id),
        'active': True,
        'start_date': {'$lte': datetime.datetime.utcnow()},
        'end_date': {'$gte': datetime.datetime.utcnow()}
    })

def like_publication(liker, liked, like):
    result = None
    liked = ObjectId(liked)
    liker = ObjectId(liker)

    if publication_by_id(liked) is not None:
        if like == 'true':
            result = publications.update({'_id': liked},
            {'$addToSet': {'likers': liker}})
        else:
            result = publications.update({'_id': liked},
            {'$pull': {'likers': liker}})
    else:
        result = {'warning' : 'publication_liked not found'}

    return result

def get_user_feed(requester, page):
    now = datetime.datetime.utcnow()

    return publications.aggregate([
        {'$match':
            {'$and':
                [
                    {'active': True},
                    {'start_date': {'$lte': now}},
                    {'end_date': {'$gte': now}},
                    {'$or':
                        [
                            {'profile_id': {'$in': requester['profiles_followed']}},
                            {'tags': {'$in': requester['tags_followed']}}
                        ]
                    }
                ]
            }
        },
        {'$lookup':
            {
                'from': 'profiles',
                'localField': 'profile_id',
                'foreignField': '_id',
                'as': 'profile'
            }
        },
        {'$project':
            {
                'profile': { '$arrayElemAt': [ '$profile', 0 ] },
                'title': 1,
                'image': 1,
                'price': 1,
                'currency': 1,
                'tags': 1,
                'quantity': 1,
                'unit': 1,
                'information': 1,
                'start_date': 1,
                'end_date': 1,
                'likers': {'$ifNull': ['$likers', []]},
                'profile_id': 1,
            }
        },
        {'$match': {'profile.status.identifier' : 1}},
        {'$sort': {'start_date': -1, 'end_date': 1, '_id': -1}},
        {'$skip': page * PUBLICATIONS_PER_FEED},
        {'$limit': PUBLICATIONS_PER_FEED},
        {'$project':
            {
                'profile_id': 0,
                'profile.profile_type': 0,
                'profile.conglomerate': 0,
                'profile.status': 0,
                'profile.profiles_followed': 0,
                'profile.tags_followed': 0,
                'profile.user.first_name': 0,
                'profile.user.last_name': 0,
                'profile.user.email': 0,
                'profile.user.password': 0,
                'profile.user.facebook_id': 0
            }
        },
        {'$addFields':
            {
                'like': {
                    '$in': [ requester['_id'], '$likers' ]
                },
                'profile.following': {
                    '$in': [ '$profile._id', requester['profiles_followed'] ]
                }
            }
        },
        {'$project':
            {'likers': 0}
        }
    ])

def search_publications_from_profiles(requester, text, profiles, page):
    ids = []
    for profile in profiles:
        ids.append(profile.get('_id'))

    return publications.aggregate([
        {'$match':
            {'$and':
                [
                    {'active': True},
                    {'start_date': {'$lte': datetime.datetime.utcnow()}},
                    {'end_date': {'$gte': datetime.datetime.utcnow()}},
                    {'profile_id': {'$in': ids}},
                    {
                        '$text': {
                            '$search': text,
                            '$language': 'portuguese',
                            '$caseSensitive': False,
                            '$diacriticSensitive': False
                        }
                    }
                ]
            }
        },
        {'$project':
            {
                'title': 1,
                'image': 1,
                'price': 1,
                'currency': 1,
                'tags': 1,
                'quantity': 1,
                'unit': 1,
                'information': 1,
                'start_date': 1,
                'end_date': 1,
                'likers': {'$ifNull': ['$likers', []]},
                'profile_id': 1,
                'score': { '$meta': 'textScore' }
            }
        },
        {'$sort': {'score': -1, 'start_date': -1}},
        {'$skip': page * PUBLICATIONS_PER_SEARCH},
        {'$limit': PUBLICATIONS_PER_SEARCH},
        {'$lookup':
            {
                'from': 'profiles',
                'localField': 'profile_id',
                'foreignField': '_id',
                'as': 'profile'
            }
        },
        {'$project':
            {
                'profile': { '$arrayElemAt': [ '$profile', 0 ] },
                'title': 1,
                'image': 1,
                'price': 1,
                'currency': 1,
                'tags': 1,
                'quantity': 1,
                'unit': 1,
                'information': 1,
                'start_date': 1,
                'end_date': 1,
                'likers': {'$ifNull': ['$likers', []]},
                'profile_id': 1,
                'score': 1
            }
        },
        {'$project':
            {
                'profile_id': 0,
                'profile.profile_type': 0,
                'profile.conglomerate': 0,
                'profile.status': 0,
                'profile.profiles_followed': 0,
                'profile.tags_followed': 0,
                'profile.user.first_name': 0,
                'profile.user.last_name': 0,
                'profile.user.email': 0,
                'profile.user.password': 0,
                'profile.user.salt': 0,
                'profile.user.facebook_id': 0,
                'language': 0
            }
        },
        {'$addFields':
            {
                'like': {
                    '$in': [ requester['_id'], '$likers' ]
                },
                'profile.following': {
                    '$in': [ '$profile._id', requester['profiles_followed'] ]
                }
            }
        },
        {'$project':
            {'likers': 0}
        }
    ])

def get_publications_from_nearby_profiles(requester, latitude, longitude, page):
    return profiles.aggregate([
        {
            '$geoNear': {
                'near': { 'type': 'Point', 'coordinates': [ longitude, latitude ] },
                'distanceField': 'dist.calculated',
                'maxDistance': REACH_RADIUS * KILOMETER,
                'query': { 'status.identifier': 1 },
                'includeLocs': 'dist.location',
                'spherical': True
            }
        },
        {'$lookup':
            {
                'from': 'publications',
                'localField': '_id',
                'foreignField': 'profile_id',
                'as': 'publications'
            }
        },
        {
            '$project': {
                'user.first_name': 0,
                'user.last_name': 0,
                'user.email': 0,
                'user.password': 0,
                'user.salt': 0,
                'user.facebook_id': 0,
            }
        },
        {
            '$project': {
                'publications._id': 1,
                'publications.title': 1,
                'publications.image': 1,
                'publications.price': 1,
                'publications.currency': 1,
                'publications.tags': 1,
                'publications.quantity': 1,
                'publications.unit': 1,
                'publications.information': 1,
                'publications.active': 1,
                'publications.start_date': 1,
                'publications.end_date': 1,
                'publications.likers': 1,
                'publications.profile._id': '$_id',
                'publications.profile.user': '$user',
                'publications.profile.address': '$address',
                'publications.profile.location': '$location',
                'publications.profile.phones': '$phones',
                'publications.dist': '$dist',
            }
        },
        {
            '$unwind' : '$publications'
        },
        {
            '$replaceRoot': { 'newRoot': '$publications' }
        },
        {'$match':
            {'$and':
                [
                    {'active': True},
                    {'start_date': {'$lte': datetime.datetime.utcnow()}},
                    {'end_date': {'$gte': datetime.datetime.utcnow()}}
                ]
            }
        },
        {'$sort': {'publications.dist.calculated' : 1, 'start_date': -1, 'end_date': 1}},
        {'$skip': page * PUBLICATIONS_PER_SEARCH},
        {'$limit': PUBLICATIONS_PER_SEARCH},
        {'$addFields':
            {
                'like': {
                    '$in': [ requester['_id'], {'$ifNull': ['$likers', []]} ]
                },
                'profile.following': {
                    '$in': [ '$profile_id', requester['profiles_followed'] ]
                }
            }
        },
        {'$project':
            {
                'likers': 0,
                'active': 0
            }
        }
    ])

def insert_publication(profile_id, publication):
    publication['start_date'] = datetime.datetime.utcfromtimestamp(publication.get('start_date')/1000)
    publication['end_date'] = datetime.datetime.utcfromtimestamp(publication.get('end_date')/1000)
    publication['profile_id'] = ObjectId(publication.get('profile_id'))

    publication.update({
        '_id': ObjectId(),
        'profile_id': ObjectId(profile_id),
        'likers': [],
        'language': 'portuguese',
        'active': True
    })

    return publications.insert_one(publication)

def update_publication(profile_id, publication):
    if publication.get('start_date'):
        publication['start_date'] = datetime.datetime.utcfromtimestamp(publication.get('start_date')/1000)

    if publication.get('end_date'):
        publication['end_date'] = datetime.datetime.utcfromtimestamp(publication.get('end_date')/1000)

    id = ObjectId(publication.get('_id'))
    publication.pop('_id', 0)
    publication['profile_id'] = ObjectId(profile_id)

    return publications.update(
        {'_id': id},
        {'$set': publication}
    )

def change_active_status(publication_id, status):
    return publications.update(
        {'_id': ObjectId(publication_id)},
        {'$set': {'active': status}}
    )
