# -*- coding: utf-8 -*-

from . import database
from pymongo import errors
from bson.objectid import ObjectId
from bson.codec_options import CodecOptions
import pytz
import tzlocal
import datetime
import logging

from ..metrics import COMPLETE_VISUALIZATION_VALUE, SIMPLE_VISUALIZATION_VALUE, VISUALIZATION_COST

statistics = database.db['statistics']
publications = database.db['publications']
logger = logging.getLogger(__name__)  # pylint: disable=invalid-name

lookup = {'$lookup':
    {
        'from': 'statistics',
        'localField': '_id',
        'foreignField': 'publication_id',
        'as': 'statistics'
    }
}

project = {'$project':
    {
        'title': 1,
        'image': 1,
        'price': 1,
        'currency': 1,
        'tags': 1,
        'quantity': 1,
        'unit': 1,
        'information': 1,
        'start_date': 1,
        'end_date': 1,
        'tags': 1,
        'visualizations': { '$sum': "$statistics.visualizations"},
        'clicks': { '$sum': "$statistics.clicks"},
        'likes': { '$size': {'$ifNull': ['$likers', []]} },
        'active': 1
    }
}

def insert_visualizations(visualizations):
    visualization_type = visualizations.get('visualization_type')

    value = {
        'COMPLETE': COMPLETE_VISUALIZATION_VALUE,
        'SIMPLE': SIMPLE_VISUALIZATION_VALUE
    }.get(visualization_type, COMPLETE_VISUALIZATION_VALUE)

    bulk = statistics.initialize_unordered_bulk_op()

    for visualization in visualizations.get('publications_visualized'):
        id = ObjectId(visualization.get('_id'))
        date = datetime.datetime.utcfromtimestamp(visualization.get('date')/1000)
        date = date.replace(minute=0, second=0, microsecond=0)

        bulk.find({
            'publication_id': id,
            'date': date
        }).upsert().update_one({
            '$inc': {'visualizations': value},
            '$setOnInsert': {
                '_id': ObjectId(),
                'publication_id': id,
                'date': date,
                'clicks': 0
            }
        })

    return bulk.execute()

def insert_clicks(clicks):
    bulk = statistics.initialize_unordered_bulk_op()

    for click in clicks.get('publications_clicked'):
        id = ObjectId(click.get('_id'))
        date = datetime.datetime.utcfromtimestamp(click.get('date')/1000)
        date = date.replace(minute=0, second=0, microsecond=0)

        bulk.find({
            'publication_id': id,
            'date': date
        }).upsert().update_one({
            '$inc': {'clicks': 1},
            '$setOnInsert': {
                '_id': ObjectId(),
                'publication_id': id,
                'date': date,
                'visualizations': 0.0
            }
        })

    return bulk.execute()

def profile_publications_statistics(id):
    return publications.aggregate([
        {'$match':
            {
                'profile_id': ObjectId(id)
            }
        },
        lookup,
        project,
        {'$sort' : {'end_date': -1, 'start_date' : -1}}
    ])

def publication_statistic(id):
    return publications.aggregate([
        {'$match':
            {
                '_id': ObjectId(id)
            }
        },
        lookup,
        project,
        {'$sort' : {'end_date': -1, 'start_date' : -1}}
    ])

def profile_general_statistics(id):
    return publications.aggregate([
        {'$match':
            {'profile_id': ObjectId(id)}
        },
        {'$lookup':
            {
                'from': 'statistics',
                'localField': '_id',
                'foreignField': 'publication_id',
                'as': 'statistics'
            }
        },
        {
            '$unwind' : '$statistics'
        },
        {'$group':
            {
                '_id': '$profile_id',
                'visualizations': {'$sum': '$statistics.visualizations'},
                'clicks': {'$sum': '$statistics.clicks'},
                'likes':  {'$sum': {'$size': '$likers'}}
             }
        },
        {'$lookup':
            {
                'from': 'profiles',
                'localField': '_id',
                'foreignField': '_id',
                'as': 'profile'
            }
        },
        {
            '$unwind' : '$profile'
        },
        {'$addFields':
            {
                'followers': '$profile.followers'
            }
        },
        {'$project': {
            'profile': 0
        }}
    ])

def profile_statics_by_period(id, type):
    date = datetime.datetime.utcnow()
    date_local = datetime.datetime.now()
    # gets the hour offset between the utc and the local time
    offset = int(round((date - date_local).total_seconds()/3600))

    grouping = '$hour'

    if type == 'year':
        # adds the offset to get the utc hour equivalent to local zero hour
        date_local = date_local.replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(hours=offset)
        grouping = '$month'
    elif type == 'month':
        # adds the offset to get the utc hour equivalent to local zero hour
        date_local = date_local.replace(day=1, hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(hours=offset)
        grouping = '$dayOfMonth'
    elif type == 'day':
        # adds the offset to get the utc hour equivalent to local zero hour
        date_local = date_local.replace(hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(hours=offset)

    return publications.aggregate([
        {'$match':
            {'profile_id': ObjectId(id)}
        },
        {'$lookup':
            {
                'from': 'statistics',
                'localField': '_id',
                'foreignField': 'publication_id',
                'as': 'statistics'
            }
        },
        {
            '$unwind' : '$statistics'
        },
        {
            '$replaceRoot': { 'newRoot': '$statistics' }
        },
        {'$match':
            {'date': {'$gte': date_local}}
        },
        {'$group':
            {
                '_id': {grouping: {
                    "$subtract": [
                        "$date", offset * 1000 * 60 * 60
                    ]
                }},
                'visualizations': {'$sum': '$visualizations'},
                'clicks': {'$sum': '$clicks'},
             }
        },
        {'$sort': {'_id': 1}}
    ])

def profile_current_month_statistics(id):
    date = datetime.datetime.utcnow()
    date_local = datetime.datetime.now()
    # gets the hour offset between the utc and the local time
    offset = int(round((date - date_local).total_seconds()/3600))

    grouping = '$month'
    endDate = date_local
    date_local = date_local.replace(day=1, hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(hours=offset)

    return publications.aggregate([
        {'$match':
            {'profile_id': ObjectId(id)}
        },
        {'$lookup':
            {
                'from': 'statistics',
                'localField': '_id',
                'foreignField': 'publication_id',
                'as': 'statistics'
            }
        },
        {
            '$unwind' : '$statistics'
        },
        {
            '$replaceRoot': { 'newRoot': '$statistics' }
        },
        {'$match':
            {'date': {'$gte': date_local}}
        },
        {'$group':
            {
                '_id': {grouping: {
                    "$subtract": [
                        "$date", offset * 1000 * 60 * 60
                    ]
                }},
                'visualizations': {'$sum': '$visualizations'}
             }
        },
        {'$addFields':
            {
                'startDate': date_local,
                'endDate': endDate,
                'status': 0,
                'visCost': VISUALIZATION_COST,
                'total': {'$multiply': ['$visualizations', VISUALIZATION_COST]},
                'test': endDate.day,
                'estimatedBillAmount': {'$multiply': [30, {'$divide': [{'$multiply': ['$visualizations', VISUALIZATION_COST]}, endDate.day]}]}
            }
        },
        {'$sort': {'_id': 1}}
    ])
