# -*- coding: utf-8 -*-
"""
Application component.
"""
from __future__ import division, absolute_import, print_function, unicode_literals

import logging

from . import server
from .api import demo
from .api import publication
from .api import profile
from .api import hashtag
from .api import statistics
from .api import report
from .api import billings

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name


class Application(object):
    """The application.

       Params:
         args - Command line arguments.
    """

    def __init__(self, args):
        self.server = server.Server(args)

        # Register the Blueprints
        self.server.register_blueprint(demo.root_bp)
        self.server.register_blueprint(demo.hello_bp)

        """Publication"""
        self.server.register_blueprint(publication.feed_bp)
        self.server.register_blueprint(publication.publication_by_profile_bp)
        self.server.register_blueprint(publication.publications_search_bp)
        self.server.register_blueprint(publication.publication_like_bp)
        self.server.register_blueprint(publication.create_publication_bp)
        self.server.register_blueprint(publication.change_active_status_bp)

        """Profile"""
        self.server.register_blueprint(profile.profile_follow_bp)
        self.server.register_blueprint(profile.login_bp)
        self.server.register_blueprint(profile.login_with_facebook_bp)
        self.server.register_blueprint(profile.sign_up_bp)
        self.server.register_blueprint(profile.profiles_suggestions_bp)
        self.server.register_blueprint(profile.add_hashtags_profile_bp)
        self.server.register_blueprint(profile.get_profile_bp)
        self.server.register_blueprint(profile.password_reset_request_bp)
        self.server.register_blueprint(profile.change_password_bp)
        self.server.register_blueprint(profile.profiles_search_bp)

        """Hashtag"""
        self.server.register_blueprint(hashtag.insert_hashtag_bp)
        self.server.register_blueprint(hashtag.get_hashtags_bp)
        self.server.register_blueprint(hashtag.search_hashtag_bp)

        """Statistics"""
        self.server.register_blueprint(statistics.visualization_bp)
        self.server.register_blueprint(statistics.click_bp)
        self.server.register_blueprint(statistics.publications_statistics_bp)
        self.server.register_blueprint(statistics.profile_statistics_by_period_bp)
        self.server.register_blueprint(statistics.profile_general_statistics_bp)

        """Report"""
        self.server.register_blueprint(report.insert_report_bp)
        self.server.register_blueprint(report.get_reports_bp)
        self.server.register_blueprint(report.get_report_types_bp)

        """Billing"""
        self.server.register_blueprint(billings.recent_bills_bp)
        self.server.register_blueprint(billings.older_bills_bp)
        self.server.register_blueprint(billings.generate_month_bills_bp)

    def run(self):
        "Run the server"
        self.server.run()
