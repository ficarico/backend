# -*- coding: utf-8 -*-
"""
    Test the API
"""

import json
import unittest

from backend import application
from backend import common

class ApiTestCase(unittest.TestCase):
    """Api test class"""
    def setUp(self):
        """ Setup the tests"""
        self.app = application.Application(common.dict_to_object({"debug": True,
                                                                  "port": 8080,
                                                                  "host": "localhost"}))
        self.client = self.app.server.test_client()

    def test_root_handler(self):
        """ Test the root handler """
        resp = self.client.get("/")
        data = json.loads(resp.data)
        self.assertEqual(200, resp.status_code)
        self.assertDictEqual(data, {"text": "Backend Application!"})

    def test_hello_handler(self):
        """ Test the hello handler """
        resp = self.client.get("/hello")
        data = json.loads(resp.data)
        self.assertEqual(200, resp.status_code)
        self.assertDictEqual(data, {"text": "Hello world!"})

        resp = self.client.get("/hello/test")
        data = json.loads(resp.data)
        self.assertEqual(200, resp.status_code)
        self.assertDictEqual(data, {"text": "Hello test!"})

if __name__ == '__main__':
    unittest.main()
