# Backend

# How to start developing

Using docker:

    docker-compose up

Create and source a virtualenv:

    mkdir virtualenv
    virtualenv virtualenv -p `which python3`
    source virtualenv/bin/activate

Install the requirements:

    pip install -r requirements.txt

The lib pysodium requires libsodium to work:

    sudo apt install libsodium-dev

Running:

    ./bin/backend

or

    make debug
